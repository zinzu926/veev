package com.veev.utils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/**
 * com.zinzu.rockrwhite.utils
 * Created by zinzu on 6/9/2016.
 */
public class Utils {

    public static String encrypt(String key,String plain){
        Blowfish blowfish=new Blowfish();
        blowfish.setKey(key);
        return blowfish.encryptString(plain);
    }

    public static String decrypt(String key,String encrypt){
        Blowfish blowfish=new Blowfish();
        blowfish.setKey(key);
        return blowfish.decryptString(encrypt);
    }

    public static String randomString(int length) {
        if (length < 1) {
            return null;
        }
        // Create a char buffer to put random letters and numbers in.
        char[] randBuffer = new char[length];
        for (int i = 0; i < randBuffer.length; i++) {
            randBuffer[i] = numbersAndLetters[randGen.nextInt(71)];
        }
        return new String(randBuffer);
    }

    private static Random randGen = new SecureRandom();

    private static char[] numbersAndLetters = ("0123456789abcdefghijklmnopqrstuvwxyz"
            + "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
}
