package com.veev.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.sql.DataSource;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static com.google.common.collect.Lists.newArrayList;
import static springfox.documentation.builders.PathSelectors.regex;

/**
 * com.zinzu.rockrwhite.config
 * Created by zinzu on 6/9/2016.
 */
@Configuration
@EnableSwagger2
@EnableTransactionManagement
@ComponentScan(basePackages = {"com.veev"})
@EnableJpaRepositories(basePackages = {"com.veev"})
public class MainConfig implements TransactionManagementConfigurer {

    private ApiKey apiKey() {
        return new ApiKey("mykey", "api_key", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.ant("/*"))
                .build();
    }

    @Bean
    public Docket userApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("veev-user")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.veev.controller"))
                .paths(regex("/users.*"))
                .build()
                .useDefaultResponseMessages(false);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Veev services API")
                .description("Veev services API")
                .license("Apache License Version 2.0")
                .licenseUrl("https://vps.rockrwhite.com/license")
                .version("1.0.0")
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope
                = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return newArrayList(
                new SecurityReference("mykey", authorizationScopes));
    }

    @Bean
    SecurityConfiguration security() {
        return new SecurityConfiguration(
                "abc",
                "123",
                "test-app-realm",
                "test-app",
                "apiKey",
                ApiKeyVehicle.HEADER,
                "api_key",
                "," /*scope separator*/);
    }

    @Bean // connection pool
    @Primary
    public DataSource dataSource() {
        HikariConfig config = new HikariConfig();
        config.setDriverClassName(env.getRequiredProperty("spring.datasource.driverClassName"));
        config.setJdbcUrl(env.getRequiredProperty("spring.datasource.url"));
        config.setUsername(env.getProperty("spring.datasource.username"));
        config.setPassword(env.getProperty("spring.datasource.password"));
        config.setConnectionTimeout(env.getProperty("spring.datasource.connectionTimeoutMillis", Long.class, TimeUnit.SECONDS.toMillis(30)));
        config.setIdleTimeout(env.getProperty("spring.datasource.idleTimeoutMillis",
                Long.class, TimeUnit.MINUTES.toMillis(5)));
        config.setMaxLifetime(env.getProperty("spring.datasource.maxLifetimeMillis",
                Long.class, TimeUnit.MINUTES.toMillis(10)));
        config.setMaximumPoolSize(env.getProperty("spring.datasource.maxPoolSize",
                Integer.class, 10));

        return new HikariDataSource(config);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean factoryBean =
                new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource());
        factoryBean.setPackagesToScan(env.getRequiredProperty("repositories.scan"));
        String ddl = env.getProperty("spring.jpa.hibernate.ddlAuto", "none");
        // vendor configuration
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setShowSql(env.getProperty("spring.jpa.showSql", Boolean.class, false));
        vendorAdapter.setGenerateDdl(!ddl.equals("none"));
        //vendorAdapter.setDatabasePlatform(Strings.nullToEmpty(env.getProperty("spring.jpa.databasePlatform")));
        vendorAdapter.setDatabase(env.getProperty("spring.jpa.database",
                Database.class, Database.DEFAULT));
        factoryBean.setJpaVendorAdapter(vendorAdapter);
        // jpa properties
        Properties additionalProperties = new Properties();
        additionalProperties.put("hibernate.hbm2ddl.auto", ddl);
        additionalProperties.put("hibernate.jdbc.fetch_size", 50);
        additionalProperties.put("hibernate.jdbc.batch_size", 100);
        // additionalProperties.put("hibernate.ejb.naming_strategy" ,
        //        env.getProperty("spring.jpa.hibernate.namingStrategy"));
        factoryBean.setJpaProperties(additionalProperties);
        return factoryBean;
    }


    @Bean
    @Primary
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }

    @Override
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        return transactionManager();
    }

    // Amazon Configuration
    @Value("${cloud.aws.credentials.accessKey}")
    private String accessKey;

    @Value("${cloud.aws.credentials.secretKey}")
    private String secretKey;

    @Value("${cloud.aws.region}")
    private String region;

    @Bean
    public BasicAWSCredentials basicAWSCredentials() {
        return new BasicAWSCredentials(accessKey, secretKey);
    }

    @Bean
    public AmazonS3Client amazonS3Client(AWSCredentials awsCredentials) {
        //amazonS3Client.setRegion(Region.getRegion(Regions.fromName(region)));
        return new AmazonS3Client(awsCredentials);
    }

    //
    @Autowired
    Environment env;
}
