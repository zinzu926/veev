/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veev.domains;

import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author zinzu
 */
@Entity
@Table(name = "v_user_branch")
public class VUsersBranch extends PersistableEntity<Long> {

    private static final long serialVersionUID = 1L;
    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private VUsers user;
    @Basic(optional = false)
    @Column(name = "branch_name")
    private String branchName;
    @ManyToOne(optional = false)
    @JoinColumn(name = "town",referencedColumnName = "id")
    private VTown town;
    @ManyToOne
    @JoinColumn(name = "region",referencedColumnName = "id")
    private VResion region;
    @Column(name = "phone")
    private String phone;
    @Column(name = "email")
    private String email;
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public VUsersBranch() {
    }

    public VUsersBranch(Long id) {
        this.id = id;
    }

    private VUsersBranch(Builder builder) {
        user = builder.user;
        branchName = builder.branchName;
        town = builder.town;
        region = builder.region;
        phone = builder.phone;
        email = builder.email;
        created = builder.created;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public VUsers getUser() {
        return user;
    }

    public String getBranchName() {
        return branchName;
    }

    public VTown getTown() {
        return town;
    }

    public VResion getRegion() {
        return region;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public Date getCreated() {
        return created;
    }

    @Override
    public String toString() {
        return toStringHelper().toString();
    }


    public static final class Builder {
        private VUsers user;
        private String branchName;
        private VTown town;
        private VResion region;
        private String phone;
        private String email;
        private Date created;

        private Builder() {
        }

        public Builder user(VUsers val) {
            user = val;
            return this;
        }

        public Builder branchName(String val) {
            branchName = val;
            return this;
        }

        public Builder town(VTown val) {
            town = val;
            return this;
        }

        public Builder region(VResion val) {
            region = val;
            return this;
        }

        public Builder phone(String val) {
            phone = val;
            return this;
        }

        public Builder email(String val) {
            email = val;
            return this;
        }

        public Builder created(Date val) {
            created = val;
            return this;
        }

        public VUsersBranch build() {
            return new VUsersBranch(this);
        }
    }
}
