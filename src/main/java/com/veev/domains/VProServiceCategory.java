/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veev.domains;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author zinzu
 */
@Entity
@Table(name = "v_pro_service_category")
public class VProServiceCategory extends PersistableEntity<Integer> {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "cat_name")
    private String catName;

    public VProServiceCategory() {
    }

    public VProServiceCategory(Integer id) {
        this.id = id;
    }

    private VProServiceCategory(Builder builder) {
        catName = builder.catName;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getCatName() {
        return catName;
    }

    @Override
    public String toString() {
        return toStringHelper().toString();
    }


    public static final class Builder {
        private String catName;

        private Builder() {
        }

        public Builder catName(String val) {
            catName = val;
            return this;
        }

        public VProServiceCategory build() {
            return new VProServiceCategory(this);
        }
    }
}
