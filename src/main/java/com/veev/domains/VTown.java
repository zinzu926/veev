/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veev.domains;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author zinzu
 */
@Entity
@Table(name = "v_town")
public class VTown extends PersistableEntity<Integer> {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "town_name")
    private String townName;

    public VTown() {
    }

    public VTown(Integer id) {
        this.id = id;
    }

    private VTown(Builder builder) {
        setTownName(builder.townName);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getTownName() {
        return townName;
    }

    public void setTownName(String townName) {
        this.townName = townName;
    }

    @Override
    public String toString() {
        return toStringHelper().toString();
    }

    public static final class Builder {
        private String townName;

        private Builder() {
        }

        public Builder townName(String val) {
            townName = val;
            return this;
        }

        public VTown build() {
            return new VTown(this);
        }
    }
}
