/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veev.domains;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author zinzu
 */
@Entity
@Table(name = "v_users_business_profile")
public class VUsersBusinessProfile extends PersistableEntity<Long> {

    private static final long serialVersionUID = 1L;
    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private VUsers user;
    @Column(name = "email")
    private String email;
    @Column(name = "address")
    private String address;
    @Column(name = "service_name")
    private String serviceName;
    @Column(name = "city")
    private String city;
    @Column(name = "hopn")
    private String hopn;
    @Column(name = "ccpn")
    private String ccpn;
    @Basic(optional = false)
    @Column(name = "opening_hours")
    private String openingHours;
    @Basic(optional = false)
    @Column(name = "closing_hours")
    private String closingHours;
    @ManyToOne(optional = false)
    @JoinColumn(name = "business_cat",referencedColumnName = "id")
    private VBusinessCategory category;
    @ManyToOne(optional = false)
    @JoinColumn(name = "product_services",referencedColumnName = "id")
    private VProServiceCategory product;
    @Basic(optional = false)
    @Column(name = "five_word")
    private String fiveWord;

    public VUsersBusinessProfile() {
    }

    public VUsersBusinessProfile(Long id) {
        this.id = id;
    }

    private VUsersBusinessProfile(Builder builder) {
        user = builder.user;
        email = builder.email;
        address = builder.address;
        serviceName = builder.serviceName;
        city = builder.city;
        hopn = builder.hopn;
        ccpn = builder.ccpn;
        openingHours = builder.openingHours;
        closingHours = builder.closingHours;
        category = builder.category;
        product = builder.product;
        fiveWord = builder.fiveWord;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public VUsers getUser() {
        return user;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getCity() {
        return city;
    }

    public String getHopn() {
        return hopn;
    }

    public String getCcpn() {
        return ccpn;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public String getClosingHours() {
        return closingHours;
    }

    public VBusinessCategory getCategory() {
        return category;
    }

    public VProServiceCategory getProduct() {
        return product;
    }

    public String getFiveWord() {
        return fiveWord;
    }

    @Override
    public String toString() {
        return toStringHelper().toString();
    }

    public static final class Builder {
        private VUsers user;
        private String email;
        private String address;
        private String serviceName;
        private String city;
        private String hopn;
        private String ccpn;
        private String openingHours;
        private String closingHours;
        private VBusinessCategory category;
        private VProServiceCategory product;
        private String fiveWord;

        private Builder() {
        }

        public Builder user(VUsers val) {
            user = val;
            return this;
        }

        public Builder email(String val) {
            email = val;
            return this;
        }

        public Builder address(String val) {
            address = val;
            return this;
        }

        public Builder serviceName(String val) {
            serviceName = val;
            return this;
        }

        public Builder city(String val) {
            city = val;
            return this;
        }

        public Builder hopn(String val) {
            hopn = val;
            return this;
        }

        public Builder ccpn(String val) {
            ccpn = val;
            return this;
        }

        public Builder openingHours(String val) {
            openingHours = val;
            return this;
        }

        public Builder closingHours(String val) {
            closingHours = val;
            return this;
        }

        public Builder category(VBusinessCategory val) {
            category = val;
            return this;
        }

        public Builder product(VProServiceCategory val) {
            product = val;
            return this;
        }

        public Builder fiveWord(String val) {
            fiveWord = val;
            return this;
        }

        public VUsersBusinessProfile build() {
            return new VUsersBusinessProfile(this);
        }
    }
}
