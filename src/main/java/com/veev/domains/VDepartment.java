/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veev.domains;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author zinzu
 */
@Entity
@Table(name = "v_department")
public class VDepartment extends PersistableEntity<Integer> {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "dep_name")
    private String depName;

    public VDepartment() {
    }

    public VDepartment(Integer id) {
        this.id = id;
    }

    private VDepartment(Builder builder) {
        depName = builder.depName;
    }

    public static Builder newBuilder() {
        return new Builder();
    }


    public String getDepName() {
        return depName;
    }


    @Override
    public String toString() {
        return toStringHelper().toString();
    }


    public static final class Builder {
        private String depName;

        private Builder() {
        }

        public Builder depName(String val) {
            depName = val;
            return this;
        }

        public VDepartment build() {
            return new VDepartment(this);
        }
    }
}
