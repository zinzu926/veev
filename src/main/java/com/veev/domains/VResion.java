/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veev.domains;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author zinzu
 */
@Entity
@Table(name = "v_resion")
public class VResion extends PersistableEntity<Integer> {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "region_name")
    private String regionName;

    public VResion() {
    }

    public VResion(Integer id) {
        this.id = id;
    }

    private VResion(Builder builder) {
        regionName = builder.regionName;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getRegionName() {
        return regionName;
    }

    @Override
    public String toString() {
        return toStringHelper().toString();
    }


    public static final class Builder {
        private String regionName;

        private Builder() {
        }

        public Builder regionName(String val) {
            regionName = val;
            return this;
        }

        public VResion build() {
            return new VResion(this);
        }
    }
}
