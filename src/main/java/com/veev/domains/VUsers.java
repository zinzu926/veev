/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veev.domains;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author zinzu
 */
@Entity
@Table(name = "v_users")
public class VUsers extends PersistableEntity<Long> {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "full_name")
    private String fullName;
    @Column(name = "city")
    private String city;
    @Column(name = "email")
    private String email;
    @Column(name = "phone")
    private String phone;
    @Column(name = "profession")
    private String profession;
    @Column(name = "image")
    private String image;
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public VUsers() {
    }

    public VUsers(Long id) {
        this.id = id;
    }

    private VUsers(Builder builder) {
        fullName = builder.fullName;
        city = builder.city;
        email = builder.email;
        phone = builder.phone;
        profession = builder.profession;
        image = builder.image;
        created = builder.created;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return toStringHelper().toString();
    }

    public String getFullName() {
        return fullName;
    }

    public String getCity() {
        return city;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getProfession() {
        return profession;
    }

    public String getImage() {
        return image;
    }

    public Date getCreated() {
        return created;
    }


    public static final class Builder {
        private String fullName;
        private String city;
        private String email;
        private String phone;
        private String profession;
        private String image;
        private Date created;

        private Builder() {
        }

        public Builder fullName(String val) {
            fullName = val;
            return this;
        }

        public Builder city(String val) {
            city = val;
            return this;
        }

        public Builder email(String val) {
            email = val;
            return this;
        }

        public Builder phone(String val) {
            phone = val;
            return this;
        }

        public Builder profession(String val) {
            profession = val;
            return this;
        }

        public Builder image(String val) {
            image = val;
            return this;
        }

        public Builder created(Date val) {
            created = val;
            return this;
        }

        public VUsers build() {
            return new VUsers(this);
        }
    }
}
