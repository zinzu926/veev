/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.veev.domains;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author zinzu
 */
@Entity
@Table(name = "v_users_depart")
public class VUsersDepart extends PersistableEntity<Long> {

    private static final long serialVersionUID = 1L;
    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private VUsers user;
    @ManyToOne(optional = false)
    @JoinColumn(name = "depart",referencedColumnName = "id")
    private VDepartment depart;
    @Basic(optional = false)
    @Column(name = "phone")
    private String phone;
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public VUsersDepart() {
    }

    public VUsersDepart(Long id) {
        this.id = id;
    }

    private VUsersDepart(Builder builder) {
        user = builder.user;
        depart = builder.depart;
        phone = builder.phone;
        email = builder.email;
        created = builder.created;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public VUsers getUser() {
        return user;
    }

    public VDepartment getDepart() {
        return depart;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public Date getCreated() {
        return created;
    }

    @Override
    public String toString() {
        return toStringHelper().toString();
    }

    public static final class Builder {
        private VUsers user;
        private VDepartment depart;
        private String phone;
        private String email;
        private Date created;

        private Builder() {
        }

        public Builder user(VUsers val) {
            user = val;
            return this;
        }

        public Builder depart(VDepartment val) {
            depart = val;
            return this;
        }

        public Builder phone(String val) {
            phone = val;
            return this;
        }

        public Builder email(String val) {
            email = val;
            return this;
        }

        public Builder created(Date val) {
            created = val;
            return this;
        }

        public VUsersDepart build() {
            return new VUsersDepart(this);
        }
    }
}
