package com.veev.repos;

import com.veev.domains.VUsersBranch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * com.veev.repos
 * Created by zinzu on 6/17/2016.
 */
@Repository
public interface VUsersBranchRepository extends JpaRepository<VUsersBranch,Long>{
}
