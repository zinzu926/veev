package com.veev.repos;

import com.veev.domains.VDepartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * com.veev.domains
 * Created by zinzu on 6/17/2016.
 */
@Repository
public interface VDepartmentRepository extends JpaRepository<VDepartment,Integer>{
}
