package com.veev.repos;

import com.veev.domains.VProServiceCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * com.veev.domains
 * Created by zinzu on 6/17/2016.
 */
@Repository
public interface VProServiceRepository extends JpaRepository<VProServiceCategory,Integer>{
}
