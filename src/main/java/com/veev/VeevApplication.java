package com.veev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VeevApplication {

	public static void main(String[] args) {
		SpringApplication.run(VeevApplication.class, args);
	}
}
