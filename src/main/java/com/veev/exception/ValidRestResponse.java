package com.veev.exception;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

/**
 * com.zinzu.rockrwhite.exception
 * Created by zinzu on 6/9/2016.
 */
public class ValidRestResponse {
    private final Object object;
    private String code;

    private ValidRestResponse(Object object) {
        this.object = Preconditions.checkNotNull(object);
        this.code = "200";
    }

    public ValidRestResponse(Object object, String code) {
        this(object);
        this.code = Preconditions.checkNotNull(code);
    }

    public static ValidRestResponse of(Object object) {
        return new ValidRestResponse(object);
    }

    public static ValidRestResponse of(Object object, String code) {
        return new ValidRestResponse(object,code);
    }

    public Object getPayload() {
        return this.object;
    }

    public String getCode() {
        return this.code;
    }
}
