package com.veev.exception;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

/**
 * com.zinzu.rockrwhite.exception
 * Created by zinzu on 6/9/2016.
 */
@ControllerAdvice
@RestController
public class GlobalExceptionHandler {

    @ExceptionHandler(value = BaseException.class)
    public ValidRestResponse handleBaseException(BaseException e) {
        return ValidRestResponse.of(Maps.newHashMap(ImmutableMap.builder().put("message", e.getMessage()).build()),"error");
    }

    @ExceptionHandler(value = Exception.class)
    public ValidRestResponse handleException(Exception e) {
        return ValidRestResponse.of(Maps.newHashMap(ImmutableMap.builder().put("message", e.getMessage()).build()),"error");
    }

}
